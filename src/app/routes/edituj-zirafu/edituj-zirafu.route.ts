import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ZirafeModel } from '../../models/zirafe.model';


@Component({
  selector: 'edituj-zirafu',
  templateUrl: './edituj-zirafu.route.html'
})
export class EditujZirafuComponent implements OnInit {

  editovanaZirafa = {}

  constructor(private route:ActivatedRoute,public model:ZirafeModel) {

    this.route.parent.params.subscribe(

      ({id}) => {
        this.model.getZirafuById(id,(zirafa) =>{
          this.editovanaZirafa = zirafa;
        });
      }
    );
  }

  updejtujZirafu (){
    this.model.updejtujZirafu(this.editovanaZirafa);
  }

 

  ngOnInit() {
  }

}
