import { Component, OnInit } from '@angular/core';
import { ZirafeModel } from '../../models/zirafe.model';

@Component({
 selector: 'nova-zirafa',
 templateUrl: './nova-zirafa.route.html',
})
export class NovaZirafaComponent implements OnInit {

 novaZirafa = {
ime:null,
kilaza:null

 }

 constructor(public model:ZirafeModel) { }


 dodajZirafu() {
   console.log(this.novaZirafa);
   if(this.novaZirafa.ime && this.novaZirafa.kilaza){
     this.model.dodajZirafu(this.novaZirafa);
   }
   
 }

 ngOnInit() {
}

}