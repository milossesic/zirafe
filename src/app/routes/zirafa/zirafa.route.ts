import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'zirafa',
  templateUrl: './zirafa.route.html'
})
export class ZirafaComponent implements OnInit {

  id;

  constructor(private router:Router,private route:ActivatedRoute ) {
    this.route.params.subscribe(({id})=>{
      this.id = id;
    })
  }

  goToPath(subpath){
    this.router.navigate(['/zirafa',this.id,subpath])
  }

  ngOnInit() {
  }

}
