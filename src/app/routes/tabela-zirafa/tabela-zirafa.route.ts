import { Component, OnInit } from '@angular/core';
import { ZirafeModel } from '../../models/zirafe.model';
import { config} from '../../config/config';
import { Router } from '@angular/router';

@Component({
  selector: 'tabela-zirafa',
  templateUrl: './tabela-zirafa.route.html'
})
export class TabelaZirafaComponent implements OnInit {
  
  searchString = "";
 conf = config.zirafeTabelaConfig;

  constructor(public model:ZirafeModel, private router:Router) { }

  ngOnInit() {
  }

  obrisiZirafu(id){
   this.model.obrisiZirafu(id);
  }

  editujZirafu(id){
    this.router.navigate(['zirafa',id,'edit'])
  }

}
