import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ZirafeModel } from '../../models/zirafe.model';

@Component({
  selector: 'detalji-zirafe',
  templateUrl: './detalji-zirafe.route.html'
 
})
export class DetaljiZirafeComponent implements OnInit {


  zirafa = {};
  constructor(private route:ActivatedRoute,public model:ZirafeModel) {
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getZirafuById(id,(zirafa)=>{
          this.zirafa = zirafa;
        });
      }
    );
  }

  ngOnInit() {

  }

}