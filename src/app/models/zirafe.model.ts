import {Injectable} from '@angular/core';
import { ZirafeService } from '../services/zirafe.service';


@Injectable()
export class ZirafeModel{

    zirafe=[];

    constructor(private service:ZirafeService){

     this.refreshujZirafe(()=> {});

    }

    refreshujZirafe(clbk){
        this.service.getAll().subscribe(

            (zirafe)=> {
                this.zirafe = zirafe;
                clbk()
            }
        );
    }

    updejtujZirafu(zirafa){
        this.service.update(zirafa).subscribe((updejtovanaZirafa)=>{
            this.refreshujZirafe(()=>{})
        })
    }

    getZirafuById(id,clbk){
        this.service.getById(id).subscribe(clbk);
    }

    getZirafuByIdCallback(id,clbk){
        if(this.zirafe.length<1){
            this.refreshujZirafe(()=>{
                for(let i = 0; i < this.zirafe.length;i++){
                    if(id == this.zirafe[i].id){
                        clbk(this.zirafe[i])
                    }
                }
            })
        } else {
            for(let i = 0; i < this.zirafe.length;i++){
                if(id == this.zirafe[i].id){
                    clbk(this.zirafe[i])
                }
            } 
        }
    }

    dodajZirafu(zirafa){
        this.service.post(zirafa).subscribe(
            (zirafa) => {
                this.zirafe.push(zirafa);
            }
        )
    }

    dajMiIndexZirafePoIdju(id,clbk){
        for(let i = 0; this.zirafe.length;i++){
            if(id == this.zirafe[i].id){
                clbk(i)
            }
        }
    }

    obrisiZirafu(id){
        this.service.delete(id).subscribe(()=> {
            this.refreshujZirafe(()=>{});
        })
    }

  


}