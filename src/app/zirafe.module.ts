import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';


import { ZirafeComponent } from './zirafe.component';
import { HeaderComponent } from './components/header/header';
import { MenuComponent } from './components/menu/menu';
import { SidebarComponent } from './components/sidebar/sidebar';
import { FooterComponent } from './components/footer/footer';
import { MenuModel } from './models/menu.model';



import { HomeComponent } from './routes/home/home.route';
import { TabelaZirafaComponent } from './routes/tabela-zirafa/tabela-zirafa.route';
import { NovaZirafaComponent } from './routes/nova-zirafa/nova-zirafa.route';
import { ZirafaComponent } from './routes/zirafa/zirafa.route';
import { EditujZirafuComponent } from './routes/edituj-zirafu/edituj-zirafu.route';
import { DetaljiZirafeComponent } from './routes/detalji-zirafe/detalji-zirafe.route';
import { MenuLinkComponent } from './components/menu-link/menu-link.component';
import { ZirafeService } from './services/zirafe.service';
import { ZirafeModel } from './models/zirafe.model';
import { FilterPipe } from './pipes/filter.pipe';
import {HighlightDirective} from './directives/highlight'


const routes:Routes = [
  {path:'',component:HomeComponent},
  {path:'dashboard',component:TabelaZirafaComponent},
  {path:'zirafe/novi',component:NovaZirafaComponent},
  {path:'zirafa/:id',component:ZirafaComponent, 
    children:[
      {path:'edit',component:EditujZirafuComponent},
      {path:'info',component:DetaljiZirafeComponent}

    ]},
 

]

@NgModule({
  declarations: [
    ZirafeComponent,
    HeaderComponent,
    MenuComponent,
    SidebarComponent,
    FooterComponent,
    HomeComponent,
    TabelaZirafaComponent,
    NovaZirafaComponent,
    ZirafaComponent,
    EditujZirafuComponent,
    DetaljiZirafeComponent,
    MenuLinkComponent,
    FilterPipe,
    HighlightDirective
   
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [
    MenuModel,
    ZirafeModel,
    ZirafeService],
  bootstrap: [ZirafeComponent]
})
export class ZirafeModule { }
