import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import{config} from '../config/config';
//import{config} from '../config';
import 'rxjs/add/operator/map';

@Injectable()
export class ZirafeService{

    constructor(private http:Http){}

    getAll(){
        return this.http
        .get(config.apiUrl+'/zirafe')
        .map((response) => {return response.json()});
       
    }
  
    getById(id){
        return this.http
        .get(config.apiUrl+'/zirafe/'+id)
        .map((response) => {return response.json()});
       
    }
  
    post(zirafa){
        return this.http
        .post(config.apiUrl+'/zirafe',zirafa)
        .map((response) => {return response.json()});
       
    }
  
    update(zirafa){
        return this.http
        .patch(config.apiUrl+'/zirafe/'+zirafa.id,zirafa)
        .map((response) => {return response.json()});
       
    }
  
    delete(id){
        return this.http
        .delete(config.apiUrl+'/zirafe/'+id)
        .map((response) => {return response.json()});
       
    }
  
    

}